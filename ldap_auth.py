import ldap
import time
import hashlib
import base64
import logging
from copy import deepcopy

from al_ui.http_exceptions import AccessDeniedException, AuthenticationException
from assemblyline.common.user_defaults import ACCOUNT_DEFAULT
from al_ui.site_specific import validate_userpass as local_validate_userpass

try:
    from typing import TYPE_CHECKING
    if TYPE_CHECKING:
        from assemblyline.al.core.datastore import RiakStore
except ImportError:
    # we don't care if this isn't here at runtime...
    pass

log = logging.getLogger('assemblyline.ldap_integration')


#####################################################
# Functions
#####################################################
class BasicLDAPWrapper(object):
    CACHE_SEC_LEN = 300
    
    def __init__(self, ldap_config):
        """

        :param ldap_config: dict containing configuration params for LDAP
        """
        self.ldap_uri = ldap_config.get("uri")
        self.base = ldap_config.get("base")
        self.uid_lookup = ldap_config.get("uid_field", "uid=%s")

        # TODO: - this isn't fully implemented
        self.group_queries = ldap_config.get("group_queries", [])
        self.group_mappings = ldap_config.get("group_mappings", {})
        self.cache = {}
        self.get_obj_cache = {}

    def create_connection(self):
        if "ldaps://" in self.ldap_uri:
            ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        l = ldap.initialize(self.ldap_uri)
        l.protocol_version = ldap.VERSION3
        l.set_option(ldap.OPT_REFERRALS, 0)
        return l


    def get_user_classification(self, uid, l=None):
        """
        Extend the users classification information with the configured group information

        NB: This is not fully implemented at this point

        :param uid:
        :param l:
        :return:
        """
        ret = ""
        for gq in self.group_queries:
            res = [x[0] for x in self.get_object(gq % uid, l)["ldap"]]
            for group_dn in res:
                if group_dn in self.group_mappings:
                    ret += self.group_mappings[group_dn]

        return ret

    def get_object(self, ldap_object, l=None):
        cur_time = int(time.time())
        cache_entry = self.get_obj_cache.get(ldap_object, None)
        if cache_entry and cache_entry['expiry'] > cur_time:
            # load obj from cache
            return {"error": None, "ldap": cache_entry['details'], "cached": True}

        if not l:
            try:
                l = self.create_connection()
            except Exception, le:
                return {"error": "Error connecting to ldapserver. Reason: %s" % (repr(le)),
                        "ldap": None, "cached": False}
        
        try:
            res = l.search_s(self.base, ldap.SCOPE_SUBTREE, ldap_object)

            # Save cache get_obj
            self.get_obj_cache[ldap_object] = {"expiry": cur_time + self.CACHE_SEC_LEN, "details": res}

            return {"error": None, "ldap": res, "cached": False}
        except ldap.UNWILLING_TO_PERFORM:
            return {"error": "Server is unwilling to perform the operation.", "ldap": None, "cached": False}
        except ldap.LDAPError, le:
            return {"error": "An error occured while talking to the server: %s" % repr(le), "ldap": None,
                    "cached": False}

    # noinspection PyBroadException
    def login(self, user, password):
        cur_time = int(time.time())
        password_digest = hashlib.md5(password).hexdigest()
        cache_entry = self.cache.get(user, None)
        if cache_entry:
            if cache_entry['expiry'] > cur_time and cache_entry['password'] == password_digest:
                cache_entry["cached"] = True
                return cache_entry
            
        try:
            l = self.create_connection()
            lookup_value = self.get_dn_from_uid(user, l=l)
            if lookup_value:
                l.simple_bind_s(lookup_value, password)
                cache_entry = {"password": password_digest, "expiry": cur_time + self.CACHE_SEC_LEN, "connection": l,
                               "details": None, "cached": False}
                self.cache[user] = cache_entry
                return cache_entry
        except Exception as e:
            # raise AuthenticationException('Unable to login to ldap server. [%s]' % str(e))
            log.exception('Unable to login to ldap server. [%s]' % str(e))
        return None

    # noinspection PyBroadException
    def get_dn_from_uid(self, uid, l=None):
        res = self.get_object(self.uid_lookup % uid, l)
        if not res['error']:
            try:
                return res['ldap'][0][0]
            except:
                return None
            
        return None

    def user_exists(self, uid, l=None):
        res = self.get_object(self.uid_lookup % uid, l)
        if not res['error']:
            if res["ldap"] and res['ldap'] != []:
                return True
            
        return False


def validate_userpass(username, password, storage):
    # type: (str, str, RiakStore) -> tuple

    # Get the configuration seed out
    seed = storage.get_blob("seed")
    ldap_config = seed.get("auth", {}).get("ldap", {})
    fail_to_local = ldap_config.get("fail_to_local", True)

    if len(ldap_config) == 0:
        raise AuthenticationException("No ldap configuration found in seed at 'auth.ldap'")

    ldap_obj = BasicLDAPWrapper(ldap_config)
    if username and password:

        ldap_info = ldap_obj.login(username, password)
        if ldap_info:
            # Make sure the user exists in AL
            if storage.get_user_account(username) is None:
                user_data = deepcopy(ACCOUNT_DEFAULT)
                user_data["uname"] = username
                storage.save_user(username, user_data)
            return username.lower(), ["R", "W", "E"]
        elif fail_to_local:
            # Fallback to internal auth
            current_user, priv = local_validate_userpass(username, password, storage)

            if current_user:
                return current_user, priv
            else:
                raise AuthenticationException("Wrong username or password (local authentication)")
        else:
            raise AuthenticationException("Wrong username or password (LDAP authentication)")

    return None, None
